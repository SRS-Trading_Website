<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>Galleri</h1>
<p>
<a href="images/MOMO_Corse.jpg">
<img src="images/MOMO_Corse_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/Antera_349.jpg">
<img src="images/Antera_349_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/Zitp_WP_876.jpg">
<img src="images/Zitp_WP_876_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/TSW_Yamara.jpg">
<img src="images/TSW_Yamara_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/TSW_MODA_VOLARE.jpg">
<img src="images/TSW_MODA_VOLARE_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/Biler_med_felger_016.jpg">
<img src="images/Biler_med_felger_016_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/Larzon_Innovation.jpg">
<img src="images/Larzon_Innovation_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/BMW_New_M5.jpg">
<img src="images/BMW_New_M5_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/PW.jpg">
<img src="images/PW_small.jpg" alt="MOMOCorse" />
</a>
<a href="images/MEGA_RS6.jpg">
<img src="images/MEGA_RS6_small.jpg" alt="MOMOCorse" />
</a>
</p>


</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
