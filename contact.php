<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->

<!--body part start -->
<div id="mainBody">

<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->

<!--right side start -->
<div id="rightPan">

<h1>Adresse</h1>
<p class="hig">SRS Trading <br/>
Kaigata 23<br/>
4280 Skudeneshavn<br/>
Tlf: 977 74 280</p>
<p class="more1"> <a href="http://www.gulesider.no/kart/map.c?q=kaigata+23&amp;imgt=MAP&amp;id=a_1078553">kart</a></p>
<p class="more1"> <a href="mailto:post@srs-trading.no">e-post</a></p>

<h1>Ansatte</h1>
<p class="hig1">Svein Arild Vikre</p>
<p class="more2"> <a href="mailto:svein.arild@srs-trading.no">e-post</a></p>

<p class="hig1">Trond Reiersen</p>
<p class="more2"> <a href="mailto:trond@srs-trading.no">e-post</a></p>

<p class="hig1">Jan Tore Sk�del</p>
<p class="more2"> <a href="mailto:jan.tore@srs-trading.no">e-post</a></p>

<p class="hig1">Karl Olav Svendsen</p>
<p class="more2"> <a href="mailto:karl.olav@srs-trading.no">e-post</a></p>

</div>
<!--right side end -->

<br class="blank" />
</div>
<!--body part end -->

<!--footer start -->
<?php include("footer.php"); ?>
<!--footer end -->

</body>
</html>
