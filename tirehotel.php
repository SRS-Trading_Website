<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>Dekkhotell</h1>
<p>Er du lei av � snuble borti dekkene dine i garasjen,
utenfor huset eller leiligheten? Er du lei av � dra dekkene
frem og tilbake hver sesong? Er du lei av alt som heter
hjulskift?
</p>

<p>Vi i SRS Trading tilbyr dekkhotell! Ved � velge dekkhotell
hos oss sikrer du at dekkene dine lagres forsvarlig,
kontrolleres og evt vaskes etter/f�r hver sesong.
Dermed slipper du at felgene blir liggende skitne og
"fallere". Videre sikrer du at dekkene dine holder
egenskapene sine lengre. Vi kan tilby 2 pakker:
</p>

<h2>Standard dekkhotell-pakke:</h2>

<ul>
<li>Kontroll av m�nsterdybde og tilstand p� dekkene</li>
<li>Kontrol av lufttrykk</li>
<li>Forskriftsmessig lagring av dekkene</li>
<li>Forsikring</li>
</ul>
<p>Pris: kr 250,- pr sesong</p>

<h2>Utvidet dekkhotell-pakke:</h2>

<ul>
<li>Kontroll av m�nsterdybde og tilstand p� dekkene</li>
<li>Kontrol av lufttrykk</li>
<li>Forskriftsmessig lagring av dekkene</li>
<li>Forsikring</li>
<li>Vasking av dekk og felger</li>
<li>Kontroll av avbalansering</li>
</ul>
<p>Pris: kr 350,- pr sesong</p>

<p style="font-size:80%">NB: Hjulskift kommer i tillegg
p� begge pakkene.
</p>

</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
