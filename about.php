<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>SRS Trading AS</h1>
<p>SRS Trading AS (org 891�326�092) ble etablert v�ren 2007 av
Jan Tore Sk�del, Trond Reiersen, Karl Olav Svendsen, og
Svein Arild Vikre. M�let med selskapet er � tilby dekk og
felgprodukter til kunder p� Haugalandet.
</p>

<p>Gjennom gode leverand�rer, og hardt arbeid har vi
opparbeidet oss en forholdsvis stor kundegruppe,
og vi arbeider hardt for at disse til enhver tid skal v�re forn�yd!
</p>

<p>Firmaets kjerneomr�det er salg av dekk, felger og
tilsvarende produkter til konkurransedyktige priser.
</p>

<p>Pr i dag s� leier vi lokaler p� ca 100 kvadratmeter i Kaigaten 23
i Skudeneshavn. V�ren 2009 flytter vi i inn i nybygg p�
ca 500 kvm p� industriomr�det Storamyr. Her vil vi f� rikelig
med plass til verksted, dekkhotell, samt en butikk hvor vi vil
ha diverse dekk, felger, ATV og diverse rekvisita p� lager.
M�lsettingen er ogs� � ha dekkene som v�re kunder ettersp�r
p� lager, for � unng� � bruke un�dig tid for kunden.
</p>

<p>
V�rt m�l  er at kundene skal f� god service, og kvalitetsprodukter
til priser som kundene er forn�yd med.
</p>

<p>Vi ser frem til � f� bes�k av DEG, dersom det er noe vi
kan hjelpe deg med!
</p>

<h1 style="text-align:center;">VELKOMMEN!</h1>

</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
