echo 'Deleting files...'
ssh w87264@uw05.uniweb.no 'rm -rf www/* www/.git*'

echo 'Uploading files...'
rsync --rsh=ssh -avz ./* .git* w87264@uw05.uniweb.no:www/
