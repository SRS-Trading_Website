<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>Dekk</h1>
<p>Vi leverer det aller meste innen dekk til person-, varebil og MC.
SRS Trading har topp moderne monteringsutstyr som sikrer at du som
kunde f�r et skikkelig godt produkt, med den kvaliteten du fortjener.
</p>

<p>Noen av merkene vi f�rer  er :
</p>

<ul>
<li>Vredestein</li>
<li>Michelin</li>
<li>Toyo</li>
<li>Cooper</li>
<li>Nokian</li>
<li>Yokohama</li>
<li>Firenza</li>
<li>Nangkang</li>
<li>Continental</li>
</ul>

<p>Mange "glemmer" at det er dekkene som er den eneste kontakten
med veibanen, og dermed viktigheten av � sikre at bilen er ustyrt
riktig ihht spesifikasjoner fra bilprodusent, samt at dekkene har
riktig m�snterdybde, kvalitet osv for � kunne ha godt grep p� vegbanen.
</p>

<p>NB:Bremselengden �ker med opptil 50% p� sommerf�re med
hel�rsdekk eller slitte sommedekk. Bremselengden �ker ogs�
betraktelig p� vinterf�re med slitte/gamle vinterdekk.
</p>

<p>
V�rt m�l  er at kundene skal f� god service, og kvalitetsprodukter
til priser som kundene er forn�yd med.
</p>

<p>Er du i tvil om tilstanden p� dekkene dine?
Kom innom for en gratis og uforpliktende tilstandskontroll!
</p>

</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
