<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>Regler og noen tips</h1>

<h2>M�nsterdybde og lovverk:</h2>

<ol>
<li>Hjul p� biler, lette varebiler og lette lastebiler
skal ha dekkm�nster som er minst 1,6 mm (sommerdekk),
3 mm (vinterdekk) dyp langs
senterlinjen og kontinuerlig rundt dekket.
</li>
<li>Dekkene m� v�re�tilpasset (dvs. de m�oppfylle
krav p�spesifikasjonene samt dimensjonen) med�hensyn
til�hvordan kj�ret�yet brukes. De m� ogs�ha den samme
luftmengden som dekkprodusenten anbefaler.
</li>
<li>Dekk av forskjellige typer f�r ikke monteres p�
samme hjulaksel (for eksempel�skal et radialdekk og
diagonaldekk�ikke monteres�p� den samme�akselen, et
kj�ret�y med to hjulaksler�skal ikke ha radialdekk p�
forakselen og diagonaldekk p� bakakselen)
</li>
</ol>

<h2>Om vinterdekk</h2>
<ol>
<li>Personbiler, lette lastebiler og busser med
totalvekt p� h�yst 3,5 ton samt�tilhenger som dras
av slikt kj�ret�y, skal v�re utstyrt med vinterdekk
eller likeverdig utstyr i perioden fra�den 1. desember
til den�31. mars.
</li>
<li>Vinterdekk uten pigger�er ikke�tilpasset kj�ring
�ret rundt�siden de er konstruert til vinterbruk. Ved
bruk�i�sommertiden�blir veigrepet mindre effektivt�mens
slitasjen �ker, samtidig�kan�dekkene f�les instabile.
</li>
<li>Nye dekk har en beskyttende glyserolfilm som bevarer
dekkene i mange �r.�Ved�bruk forsvinner glyserolen og
gummiblandingen t�rker hurtigere. Dette f�rer til at�dekkene
har�d�rligere kj�reegenskaper og kortere levetid. Oppbevares
dekkene under riktige vilk�r, dvs.�i m�rket og i en jevn
kj�lig temperatur kan de bli opptil 10 �r gamle og fremdeles
ha gode kj�reegenskaper.�Vedr�rende vinterdekk�blir veigrepet
mindre effektivt�p� is og sn� allerede etter 5-6 �r. Dette
kommer an p�type gummiblanding som de inneholder. Det kan
v�re forskjell p� dekkmerkene.�Sjekk det med din dekkforhandler.
</li>
<li>Dersom�en tilhenger�trekkes av�en bil med�totalvekt p�
h�yst 3500 kg, som har p�monterte�piggdekk, skal tilhengeren
ogs� ha piggdekk.
</li>
<li>Dekkene med den st�rste m�nsterdybden skal v�re montert bak.
</li>
</ol>

<p>Lettmetallfelger skal etter montering p� din bil kontrolleres
og�strammes�hvis det finnes en liten risiko for at boltene kan
l�snes etter noen titalls mil. Det er ditt ansvar som f�rer �
sjekke om dette er i forsvarlig orden p� de biler du f�rer eller skal f�re.
</p>

<h2>N�r m� jeg bytte dekk?</h2>

<p>For de tre nordligste fylker gjelder:</p>
<ul>
<li>Piggdekk kan benyttes fra og med 15. oktober til 1. mai.
</li>
</ul>

<p>For resten av Norge gjelder:</p>
<ul>
<li>Piggdekk kan benyttes fra og med 1. november til
og med f�rste s�ndag etter p�ske.
</li>
</ul>

<p>Vinterdekk, pigget eller upigget, er merket M+S (Mud and Snow).
<a href="http://www.conti-online.com/generator/www/no/no/continental/automobile/themes/dekktips/helarsdekk_no.html">"Hel�rsdekk"</a>
betraktes ikke som vinterdekk, selv
om de er merket M+S. MERK: BREMSELENGDEN �KER med opptil 50%
p� sommerf�re med hel�rsdekk!
</p>

<h2>Vedlikehold og vask</h2>

<p>Det er viktig � rengj�re dekkene skikkelig f�r de legges
til oppbevaring for neste sesong, spesielt gjelder dette for
vinterdekk. Bruk egnet dekkvask (emulgerende) som du f�r kj�pt
p� bensinstasjoner. Vask deretter med s�pe og skyll med varmt
vann. Ikke kj�r i kald sn� rett etter at middelet er sprayet
p�, dette blir som � vaske olje av hendene med kaldt vann!
</p>

<h2>Dekkets alder</h2>

<p>Alle dekk har den s�kalte DOT-koden p� sideveggen.
Denne koden forteller n�r dekket er produsert. Etter �r
2000 har koden fire siffere i stedet for tre som tidligere.
De to f�rste siffrene angir produksjonsuke og de to siste
produksjons�r. For eksempel: DOT-koden 0304, som forteller at dekket
er produsert i uke 3 i �r 2004.
</p>

<h2>Lufttrykk</h2>

<ul>
<li>Sjekk lufttrykk og dekkenes almentilstand jevnlig,
f.eks annen hver uke. Riktig lufttrykk finner du i
instruksjonsboken, inne i d�ren p� f�rerside, eller i tanklokk.
</li>
<li>Dekket skal ha normal temperatur n�r du m�ler og justerer
lufttrykket. Vent derfor noen timer hvis dekket har v�rt utsatt
for h�y fart eller stor belastning.
</li>
<li>Du b�r ha 10% mer luft i vinterdekk enn i sommerdekk.
Dette fordi nordiske dekk med skarp skulder og myk gummi er
mer �mfintlige for spor i veibanen ved lavt lufttrykk, i tillegg
fortetter luft seg i kulde.
</li>
<li>Glem heller ikke at fullastet bil krever mer luft i dekkene.
</li>
<li>Ventilhettene m� skrus skikkelig til, siden de skal beskytte
ventilen mot vann og skitt og selvf�lgelig ogs� mot synkende lufttrykk.
</li>
<li>Husk ogs� � kontrollere reservehjulet.
</li>
</ul>

</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
