<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>Tiden snart inne for vinterdekk</h1>
<p>Ja, s� var tiden snart inne for � klargj�re vinterdekkene igjen...
Regelverket sier at det er tillatt � kj�re med piggdekk fra f�rste november til f�rste s�dag etter p�ske.
(NB: Skal alltid sko deg etter f�ret, uansett tid).</p>

<ul>
<li>Har du sjekket vinterdekkene dine?</li>
<li>Trenger du nye dekk?</li>
<li>Er du i tvil om tilstanden p� dekkene dine,
 eller �nsker en pris p� nye dekk eller felger?</li>
</ul>
<p>Vi st�r til din disposisjon for kontroll av dekk eller pris.</p>
<p style="text-align:center; font-size:75%;">�pningstider:<br/>
Mandag, Tirsdag, Onsdag, Fredag: 09:00-17:00<br/>
Torsdag : 09:00-19:00<br/>
L�rdag: 10:00-14:00<br/>
I sesongen har vi utvidede �pningstider.<br/>
<img src="images/BankAxept_108x86.gif" alt="Bank Axept" />
</p>

</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
