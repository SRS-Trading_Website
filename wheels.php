<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>SRS Trading</title>
<link href="style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--header paer start -->
<div id="headerPan">
<?php include("header.php"); ?>
</div>
<!--header part end -->
<!--body part start -->
<div id="mainBody">
<!--left side start -->
<div id="leftPan">
<?php include("left_pane.php"); ?>
</div>
<!--left side end -->
<!--right side start -->
<div id="rightPan">
<h1>Felger</h1>
<p>SRS Trading AS leverer det aller meste innen felg.
V�rt leverand�rnettverk er bredt, og det tar som regel
ikke mer enn et par dager for � fremskaffe felger som
vi evt ikke har p� lager. Trenger bilen din et "ansiktsl�ft"?
Da skal du ta turen innom oss, s� kan vi skaffe en god og
konkurransedyktig pris p� nye felger til din bil.
Dersom du er usikker p� hva som passer din bil, st�r
vi til din disposisjon, og vil gi deg anbefaling og r�d,
slik at du kan finne de felgene som passer nettopp din bil.
</p>

<p>Noen av merkene vi f�rer  er :
</p>

<ul>
<li>AEZ</li>
<li>DOTZ</li>
<li>DEZENT</li>
<li>ENZO</li>
<li>ZITO</li>
<li>WEDS</li>
<li>ANTERA</li>
<li>ALUTEC</li>
<li>BBS</li>
<li>BORBET</li>
<li>MEGA</li>
</ul>

</div>
<!--right side end -->
<br class="blank" />
</div>
<!--body part end -->
<!--footer start -->

<?php include("footer.php"); ?>

<!--footer end -->
</body>
</html>
